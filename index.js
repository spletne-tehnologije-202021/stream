const http = require('http');
const fs = require('fs');

http.createServer((req, res) => {
    res.writeHead(200, { 'Content-type': 'video/mp4' });
    const stream = fs.createReadStream('btrailer.mp4');
    stream.pipe(res);
}).listen(4000, () => {
    console.log('Strežnik posluša na http://localhost:4000');
});